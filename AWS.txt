AWS

Amazon Web Services

AWS provides IT infrastructure like CPU, Storage, networking in form of service.

Core Benefits

- Low Cost
- Instant Elasticity
- Scalability
- Multiple OS's
- Multiple Storage Options
- Secure

AWS Global Infrastructure

- Regions 
- Availability Zones

Accessing AWS Platform

- AWS Management Console
- CLI (Command Line Interface)
- SDKs (Software Development Kits)

Most Commonly used AWS Services

- Compute and Networking Services
	- Elastic Compute Cloud (EC2)
	- Lambda	
	- Auto Scaling
	- Elastic Load Balancing
	- Elastic Beanstalk
	- Virtual Private Cloud
- Database Services
	- Relational Database Service (RDS)
	- Dynamo DB
	- Redshift
	- Elasticcache
- Storage and Content Delivery Services
	- Elastic Block Storage (EBS)
	- Simple Storage Service (S3)
	- Cloud Front
- Management Tools and Services
	- Cloudwatch
	- Cloudformation
	- Cloudtrail
- Security and Identity Services
	- Identity and Access Management (IAM)
- Application Services
	- API Gateway
	- Simple Notification Service
	- Simple Queue Service


--------
EC2 

- Key Concepts
	- AMIs (Amazon Machine Images)
	- Instance Type
	- Security Groups
	- Key Pair

- Creating an EC2 Instance (Windows)
	- Login into AWS Management Console
	- Search for EC2 in the search bar
	- Click on Launch Instance
	- Under AMI Selection Page - Select Microsoft Windows Server 2019 Base 
	- Choose Instance Type as t2.micro
	- Click Review and Launch
	- Click on Launch
	- Under a drop down in key selection prompt, select Create a new Key Pair
	- Define a name
	- Download Key Pair
	- Click on Launch Instances
	- Click on View Instances, to get landed to Instances List


- Log in to Windows Instance
	- Note the Public IP address of the VM
	- Select the instance, click on Actions -> Security -> Get Windows Password
	- On your windows laptop, under start menu, search for Remote Desktop Connection and open that.
	- In the dialog box, paste the public IP of instance copied
	- Now before providing credentials, More Choices -> Use different account
	- Provide your username and password, which you got from AWS and click on ok.
	- Now you will see, you got logged into the server.

- Creating an EC2 Instance (ubuntu)
	- Login into AWS Management Console
	- Search for EC2 in the search bar
	- Click on Launch Instance
	- Under AMI Selection Page - Select Ubuntu Server 18.04 LTS (HVM) 
	- Choose Instance Type as t2.micro
	- Click Review and Launch
	- Click on Launch
	- Under a drop down in key selection prompt, select Select an existing key pair
	- From the dropdown, select the key pair, which you created in the previous lab
	- Check the box stating the acknowledgment
	- Click on Launch Instances
	- Click on View Instances, to get landed to Instances List

- Login using Putty
	- Converting pem to ppk
		- To convert the key in putty compatible format, open PuttyGen
		- Click on Load
		- Change the filter to All Files in bottom right corner.
		- Browse the pem file which you downloaded while creating the VM
		- Click ok
		- Save private key
		- Click yes
		- Now save the file in any location with any name
		- That's it, now you have a private key in Putty compatible format(ppk)

	- In order to login
		- Just open putty
		- Paste your IP in hostname section.
		- In the left pane, expand SSH, click on Auth.
		- In the right side, browse ppk while which you got from puttygen
		- Click open
		- Just provide your username when prompted, (ubuntu)
 		- That's it, you have successfully logged into the server

Installing Web Server on Ubuntu based Ec2 Instance

- Update Repo List
	apt-get update

- Install Apache2
	apt-get install apache2

- To browse Apache2 using Command line
	curl localhost

- To allow the traffic on EC2 instance on port 80
	- Go to instance page
	- select your instance
	- In the bottom pane, click on Security
	- Click on security group name, it will take you to the security group page
	- Click on Edit Inbound Rules
	- Add Rule
	- Put 80 in Port Range
	- In source, select Anywhere
	- Click on Save Rules


