Default Inventory File - /etc/ansible/hosts

Default Ansible Connfiguration File - /etc/ansible/ansible.cfg



Ansible Installation and Initial Setup

- On Master
	- Switch to root user
		sudo su
	- Change the hostname
		hostnamectl set-hostname master
	- To make the change visible in console, just log out from the root user once, and log in again, you will see the difference
		exit
		sudo su
	- Update the repo list
		apt-get update
	- Add repository path
		add-apt-repository ppa:ansible/ansible
	- Install ansible
	- Verify Installation
		ansible --version
	- Generate the key pair
		ssh-keygen
	- During the execution of above command, it will prompt you for few inputs, just keep them default by pressing enter key
	- Now, we do have key pair generated, let's copy the public key and add it into the managed node
		cat /root/.ssh/id_rsa.pub
	- Now copy the content which came as output of above command

- On managed node
	- Switch to root user
		sudo su
	- Change the hostname
		hostnamectl set-hostname node
	- To make the change visible in console, just log out from the root user once, and log in again, you will see the difference
		exit
		sudo su
	- Let's add the public key which we copied from master
		vi /root/.ssh/authorized_keys
	- Now paste the key which you copied from master

- To test the connection
	- On master
		ssh root@private_ip_of_managed_node
	- If you got inside the shell of managed node, it means, you have successfully exchanged the keys, if not you have to look out and fix it.


Setting up Ansible Inventory

- Update the inventory file
	vi /etc/ansible/hosts
- You can add the managed node in this file as below
	[lab]
	private_ip_of_your_managed_node
- This will add your node in inventory file, and if you need to refer to this machine from you commands or playbooks, you will make use of group name, rather than using complete ip again and again

Communication Test with Ansible

- ansible name_of_host/group -m ping
	Eg. ansible lab -m ping

Ansible Documentation

- To list all the modules
	ansible-doc -l
- To know more about a module
	ansible-doc module_name

- To close the documentation utility just press q.

Adhoc Commands

- Directory
	ansible lab -m file -a "name=/tmp/dir1 state=directory"
- File
	ansible lab -m file -a "name=/tmp/file1 state=touch"
- User
	ansible lab -m user -a "name=thinknyx state=present"
- User with did
	ansible lab -m user -a "name=thinknyx uid=1003 state=present"
- Install package
	ansible lab -m apt -a "name=telnet state=present"

- To get the facts
	ansible lab -m setup

Lab1
- Create a user named support with uid 2234 and shell - /bin/bash
- Create a directory (/tmp/demodir), owner of file should be "support"
- Create a file named demofile in the directory created in above step, owner of file should be "support" and mode - 0755
- Install a package named ntp

Solution
- ansible lab -m user -a "name=support uid=2234 shell=/bin/bash"
- ansible lab -m file -a "name=/tmp/demodir owner=support state=directory"
- ansible lab -m file -a "name=/tmp/demodir/demofile owner=support mode=0755 state=touch"
- ansible lab -m apt -a "name=ntp state=present update_cache=true"

Lab2

Create a playbook to Install Apache2
Your playbook should update the default web page which your apache2 is serving (/var/www/html/index.html)

Solution

---
- hosts: lab
  tasks:
    - name: install apache2
      apt:
        name: apache2
        state: present
        update_cache: true
    - name: copy a file
      copy:
        src: index.html
        dest: /var/www/html/
