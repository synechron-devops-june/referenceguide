## Some Reference Links
* Git Cheatsheet
    * https://thinknyx.com/git-cheat-sheet/
* AWS Reference Guide
    * https://thinknyx.com/category/aws/
* Docker Cheatsheet
    * https://thinknyx.com/docker-cheat-sheet/
* Docker
    * https://www.youtube.com/watch?v=I9-1WuS6N0c&list=PLW8sN1d4Uj-9C3FzBWwg7z1hIjMx78JaU
* DevOps
    * https://www.youtube.com/watch?v=bKniuzTMhok&list=PLW8sN1d4Uj-_LYfFptMRf4l2A8hApP71D
